module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    parserOptions: {
        parser: 'babel-eslint'
    },
    extends: [
        '@nuxtjs',
        'plugin:nuxt/recommended'
    ],
    plugins: [],
    // add your custom rules here
    rules: {
        'no-console': 'off',
        'no-debugger': 'off',

        'nuxt/no-cjs-in-config': 'off',
        'quotes': ['error', 'single'],
        'indent': ['error', 4],

        'comma-dangle': [
            'warn',
            'always-multiline',
        ],
        'semi': ['warn', 'always'],
        'template-curly-spacing': ['warn', 'always'],
        'no-unused-vars': 'warn',
        'import/no-mutable-exports': 'off',
        'vue/html-indent': ['warn', 4, {
            'attribute': 1,
            'baseIndent': 1,
            'closeBracket': 0,
            'alignAttributesVertically': true,
            'ignores': [],
        }],
        'vue/no-v-html': 'off',
        'vue/multiline-html-element-content-newline': 'off',
        'vue/singleline-html-element-content-newline': 'off',
        'vue/html-self-closing': 'off',
    },
}
